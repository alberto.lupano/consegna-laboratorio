# GIT COMAND HANDBOOK

Tutti i comandi di Git con relative spiegazioni

## git help
Permette di accedere alla documentazione dei comandi

## git config
Permette di inserire username e email per configurare git prima di poterlo utilizzare. Può essere usato con l'aggiunta dell'opzione "--global" per modificare l'intero spazio locale

## git init
Creare una nuova repository vuota.

## git clone
Clona una repository già esistente in una nuova repository.

## git status
Visualizza lo stato dei file, quali sono stati tracciati e quali no

## git add
Aggiunge un nuovo file in preparazione del nuovo commit

## git commit
Crea una nuova istantanea* con il contenuto corrente dell'indice. Bisogna inserire una descrizione che permetta di capire in quale momento della storia del progetto si trova.

*istantanea: un salvataggio in un preciso momento nella storia del progetto.

## git log
Permette di vedere l'elenco dei commit effettuati in passato. Si possono aggiungere le opzioni:

* "--oneline": Visualizza i commit solo su una riga con le informazioni essenziali
* "--all": Visualizza tutti i commit anche quelli effettuati da remoto.
* "--graph": Visualizza l'ordine dei commit a forma di grafico per evidenziare i branch
* "-stat": Visualizza i commit con tutte le informazioni che sono salvate su di essi.

## git restore
Ripristina un file all'ultimo commit effettuato eliminando le ultime modifiche.

## git diff
Visualizza le differenze tra i file modificati e i file al momento dell'ultimo commit.

## git remote
Permette di visualizzare i repository remoti configurati. Con l'opzione "add" permette di aggiungerli.

## git push
Carica gli aggiornamenti del repository locale al repository remoto.

## git pull
Il comando inverso di git push, carica gli aggiornamenti del repository remoto sul repostiroy locale.

## git fetch
Stessa funzione di git pull ma non unisce automaticamente le modifiche, ti permette di visualizzare i cambiamenti senza modificare il lavoro locale.

## git branch
Permette di creare, elencare, rinominare e rinominare branch. Sono rami che permettono di lavorare in modo parallelo al progetto.

## git checkout
Permette di cambiare il branch corrente, così da lavorare su altri rami.

## git merge
Permette di unire i cambiamenti di due branch per ottenere i file aggiornati da entrambi i lavori separati.

## git rebase
Serve per aggiungere i commit di un branch su un altro. Serve per evitare troppe ramificazioni e mantenere la cronologia lineare.

## .gitignore
Questo file serve per specificare file o cartelle che non vengono tracciati dal sistema di controllo.
